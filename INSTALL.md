# Installation instructions

## Table of Contents

1. Requirements
2. Installation
3. Configuration

### 1. Requirements

* Apache HTTP Server 2.x
* PHP 7.x

### 2. Installation

Extract the release zip file into your webservers base
directory. Place your content pages into the folder content. Add the
following _mod_rewrite_ rules to your base directory _.htaccess_ file.

```
RuleEngine On
RewriteRule TBD
```

### 3. Configuration
