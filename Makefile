
DEPENDENCIES=$(TARGET_DIR)/anatcms.tar.gz
SOURCE_DIR=src
TARGET_SRC=$(TARGET_PHP) $(TARGET_MO) $(TARGET_DOC) $(TARGET_APACHE) $(TARGET_STYLE)
TARGET_DIR=target
HTDOCS_DIR=$(TARGET_DIR)/htdocs

TARGET_PHP=\
	$(HTDOCS_DIR)/anat/cms/api/controller.php\
	$(HTDOCS_DIR)/anat/cms/conf/config.php\
	$(HTDOCS_DIR)/anat/cms/content/welcome.php\
	$(HTDOCS_DIR)/anat/cms/error/error-no-site-requested.php\
	$(HTDOCS_DIR)/anat/cms/error/error-site-not-found.php\
	$(HTDOCS_DIR)/anat/cms/lib/autoloader.php\
	$(HTDOCS_DIR)/anat/cms/lib/contentmanagementsystem.php\
	$(HTDOCS_DIR)/anat/cms/lib/templateengine.php\
	$(HTDOCS_DIR)/anat/cms/lib/language.php\
	$(HTDOCS_DIR)/anat/cms/templates/default-footer.php\
	$(HTDOCS_DIR)/anat/cms/templates/default-header.php\
	$(HTDOCS_DIR)/anat/cms/templates/default-head.php

TARGET_MO=\
	$(HTDOCS_DIR)/anat/cms/locale/de/LC_MESSAGES/messages.mo\
	$(HTDOCS_DIR)/anat/cms/locale/en/LC_MESSAGES/messages.mo

TARGET_DOC=\
	$(HTDOCS_DIR)/COPYING\
	$(HTDOCS_DIR)/INSTALL.md\

TARGET_APACHE=\
	$(HTDOCS_DIR)/.htaccess

TARGET_STYLE=\
	$(HTDOCS_DIR)/anat/cms/styles/default-theme.css

.PHONY: all clean

all: $(DEPENDENCIES)

$(HTDOCS_DIR):
	mkdir -p $(HTDOCS_DIR)
	mkdir -p $(HTDOCS_DIR)/anat/cms/api
	mkdir -p $(HTDOCS_DIR)/anat/cms/conf
	mkdir -p $(HTDOCS_DIR)/anat/cms/content
	mkdir -p $(HTDOCS_DIR)/anat/cms/error
	mkdir -p $(HTDOCS_DIR)/anat/cms/lib
	mkdir -p $(HTDOCS_DIR)/anat/cms/styles
	mkdir -p $(HTDOCS_DIR)/anat/cms/templates
	mkdir -p $(HTDOCS_DIR)/anat/cms/locale/de/LC_MESSAGES
	mkdir -p $(HTDOCS_DIR)/anat/cms/locale/en/LC_MESSAGES

$(TARGET_DIR)/anatcms.tar.gz: $(TARGET_SRC)
	tar cvzf $(TARGET_DIR)/anatcms.tar.gz -C $(HTDOCS_DIR) anat COPYING INSTALL.md .htaccess

$(HTDOCS_DIR)/anat/cms/api/%.php: $(SOURCE_DIR)/anat/cms/api/%.php | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)/anat/cms/api

$(HTDOCS_DIR)/anat/cms/conf/%.php: $(SOURCE_DIR)/anat/cms/conf/%.php | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)/anat/cms/conf

$(HTDOCS_DIR)/anat/cms/content/%.php: $(SOURCE_DIR)/anat/cms/content/%.php | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)/anat/cms/content

$(HTDOCS_DIR)/anat/cms/error/%.php: $(SOURCE_DIR)/anat/cms/error/%.php | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)/anat/cms/error

$(HTDOCS_DIR)/anat/cms/lib/%.php: $(SOURCE_DIR)/anat/cms/lib/%.php | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)/anat/cms/lib

$(HTDOCS_DIR)/anat/cms/templates/%.php: $(SOURCE_DIR)/anat/cms/templates/%.php | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)/anat/cms/templates

$(HTDOCS_DIR)/anat/cms/styles/%.css: $(SOURCE_DIR)/anat/cms/styles/%.css | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)/anat/cms/styles

$(HTDOCS_DIR)/anat/cms/locale/de/LC_MESSAGES/messages.mo: $(SOURCE_DIR)/anat/cms/locale/de/LC_MESSAGES/messages.po | $(HTDOCS_DIR)
	msgfmt $< -o $@

$(HTDOCS_DIR)/anat/cms/locale/en/LC_MESSAGES/messages.mo: $(SOURCE_DIR)/anat/cms/locale/en/LC_MESSAGES/messages.po | $(HTDOCS_DIR)
	msgfmt $< -o $@

$(HTDOCS_DIR)/COPYING: COPYING | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)

$(HTDOCS_DIR)/INSTALL.md: INSTALL.md | $(HTDOCS_DIR)
	cp $< $(HTDOCS_DIR)

$(HTDOCS_DIR)/.htaccess: $(SOURCE_DIR)/.htaccess | $(HTDOCS_DIR)
	cp $< $@

clean:
	rm -rf $(TARGET_DIR)
