# AnatCMS

The meaning of Anat:
1. Acronym for "All names are taken"
2. Egyptian goddess

This content management system is part of a little case study for
writing a website from scratch. It is basically a simple templating
engine with support for internationalization.
