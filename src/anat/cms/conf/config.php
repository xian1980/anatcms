<?php

namespace Anat\CMS;

const SUPPORTED_LANGUAGES = [
    "de" => "de_AT",
    "en" => "en_GB"
];

const DEFAULT_LANGUAGE = "en";
