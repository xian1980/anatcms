<!DOCTYPE html>

<?php
require_once "../lib/autoloader.php";

spl_autoload_register("\Anat\CMS\loadClass");

$cms = new Anat\CMS\ContentManagementSystem($_SERVER['HTTP_ACCEPT_LANGUAGE'], $_GET);
?>

<html lang="<?= $cms->getLanguage() ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= _("Titel") ?></title>
    <?php include_once($cms->getHead()); ?>
</head>

<body>
    <?php include_once($cms->getHeader()); ?>
    <?php include_once($cms->getContent()); ?>
    <?php include_once($cms->getFooter()); ?>
</body>

</html>
