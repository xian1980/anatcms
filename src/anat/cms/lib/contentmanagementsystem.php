<?php

namespace Anat\CMS;

final class ContentManagementSystem
{
    private $language;
    private $content;

    public function __construct(?string $httpAcceptLanguage, ?array $get)
    {
        $this->language = new Language($httpAcceptLanguage);
        $this->content = new Content($get);
    }

    public function getLanguage() : ?string
    {
        return $this->language->getLanguage();
    }

    public function getHeader() : ?string
    {
        return $this->content->getHeader();
    }

    public function getFooter() : ?string
    {
        return $this->content->getFooter();
    }

    public function getContent() : ?string
    {
        return $this->content->getRequestedSite();
    }
}
