<?php

namespace Anat\CMS;

function loadClass($class)
{
    require_once strtolower(preg_replace("~^Anat\\\\CMS\\\\([A-Za-z]+)$~", "$1.php", $class));
}
