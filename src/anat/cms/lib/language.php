<?php

namespace Anat\CMS;

require_once "../conf/config.php";

final class Language
{
    private const LOCALE_DIRECTORY = "../locale";

    public $language = DEFAULT_LANGUAGE;

    public function __construct($httpAcceptLanguage)
    {
        $language = substr($httpAcceptLanguage, 0, 2);

        if (array_key_exists($language, SUPPORTED_LANGUAGES)) {
            $this->language = $language;
        }
        
        $locale = SUPPORTED_LANGUAGES[$language];

        setlocale(LC_ALL, $locale);
        bindtextdomain("messages", self::LOCALE_DIRECTORY);
        textdomain("messages");
    }

    public function getLanguage() : ?string
    {
        return $this->language;
    }
}
