<?php

namespace Anat\CMS;

final class TemplateEngine
{
    private $error = null;
    private $site  = null;

    public function __construct(?array $queryParams = array())
    {
        // Verification of requested content. Only sites from the content
        // directory are allowed.

        if (array_key_exists("site", $queryParams)) {
            $contentPages = array();
            foreach (glob(__DIR__."/../content/*.php") as $contentPage) {
                $contentPages[] = preg_replace("~^.*/content/([a-z-]+)\.php$~", "$1", $contentPage);
            }
            $this->site = $queryParams["site"];
            if (!in_array($this->site, $contentPages)) {
                $this->error = "error-site-not-found";
            }
        } else {
            $this->error = "error-no-site-requested";
        }
    }

    public function getHead() : ?string
    {
        $template = $this->getHeadTemplate($this->site);
        if (file_exists($template)) {
            return $template;
        } else {
            return $this->getHeadTemplate("default");
        }
    }

    public function getHeader() : ?string
    {
        $template = $this->getHeaderTemplate($this->site);
        if (file_exists($template)) {
            return $template;
        } else {
            return $this->getHeaderTemplate("default");            
        }
    }

    public function getFooter() : ?string
    {
        $template = $this->getFooterTemplate($this->site);
        if (file_exists($template)) {
            return $template;
        } else {
            return $this->getFooterTemplate("default");            
        }
    }

    public function getSite() : ?string
    {
        if ($this->isErroneous()) {
            return $this->getErrorTemplate($this->error);
        } else {
            return $this->getContentTemplate($this->site);
        }
    }

    private function getHeadTemplate(?string $site) : ?string
    {
        return $this->getFile("templates", $site, "-head");
    }

    private function getHeaderTemplate(?string $site) : ?string
    {
        return $this->getFile("templates", $site, "-header");
    }

    private function getFooterTemplate(?string $site) : ?string
    {
        return $this->getFile("templates", $site, "-footer");
    }

    private function getErrorTemplate(?string $site) : ?string
    {
        return $this->getFile("error", $site);
    }

    private function getContentTemplate(?string $site) : ?string
    {
        return $this->getFile("content", $site);
    }

    private function getDir(?string $baseDir, ?string $site) : ?string
    {
        return "../$baseDir/$site";
    }

    private function getFile(?string $baseDir, ?string $site, ?string $postfix = "") : ?string
    {
        return $this->getDir($baseDir, $site).$postfix.".php";
    }

    private function isErroneous() : ?bool
    {
        return $this->error != null;
    }
}
